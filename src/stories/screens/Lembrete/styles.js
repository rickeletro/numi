import { Platform, Dimensions} from "react-native";
export default {
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20,
    color: "#fff"
  },
  label: {
    color: '#ccc',
    fontSize: 16,
    marginTop: 10,
  },
  contentContainer: {
    backgroundColor: "#EDA04C",
  },
  view: {
    height: 250,
    backgroundColor: "#EDA04C",
    flex: 1,
    paddingTop: 15,
    paddingBottom: 15,
  },
  cron: {
    fontSize: 50,
    color: "#fff"
  }
};
