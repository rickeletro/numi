// @flow
import * as React from "react";
import PraticaGratuita from "../../stories/screens/PraticaGratuita";
export interface Props {
	navigation: any,
}
export interface State {}
export default class PraticaGratuitaContainer extends React.Component<Props, State> {
	render() {
		return <PraticaGratuita navigation={this.props.navigation} />;
	}
}
