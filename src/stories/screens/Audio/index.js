import * as React from "react";
import { WebView, Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Card,
	CardItem,
	ListItem,
	Thumbnail,
	Text,
	Left,
	Body,
	Right,
	List
} from "native-base";
import styles from "./styles";
class Audio extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			datas: []
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	componentDidMount() {
		fetch('http://app.nucleonumi.com.br/wp-json/wp/v2/material_de_curso?tipo_de_material=21')
			.then(response => {
				return response.json();
			})
			.then(resp => {
				this.setState({
					datas: resp
				});
			});
	}
	render() {
		// const param = this.props.navigation.state.params;
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ alignItems: "center" }}>
						<Title>Audios</Title>
					</Body>

					<Right />
				</Header>

				<Content padder>
					<List
						dataArray={this.state.datas}
						renderRow={data =>
							<ListItem avatar button
								onPress={() => this.props.navigation.navigate('ViewAudio', data)}
							>
								<Left>
									<Thumbnail style={{ width: 32, height: 32 }} source={require('../../../../assets/play-button.png')} />
								</Left>
								<Body>
									<Text>{data.title.rendered}</Text>
									<Text note>{data.content.rendered.stripHTML()}</Text>
								</Body>
								<Right>
									<Text note>0:00</Text>
								</Right>
							</ListItem>
						}
					/>
				</Content>
			</Container>
		);
	}
}

export default Audio;
