// @flow
import * as React from "react";
import Perguntas from "../../stories/screens/Perguntas";
export interface Props {
	navigation: any,
}
export interface State {}
export default class PerguntasContainer extends React.Component<Props, State> {
	render() {
		return <Perguntas navigation={this.props.navigation} />;
	}
}
