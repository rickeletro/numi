// @flow
import * as React from "react";
import Diario from "../../stories/screens/Diario";
export interface Props {
	navigation: any,
}
export interface State {}
export default class DiarioContainer extends React.Component<Props, State> {
	render() {
		return <Diario navigation={this.props.navigation} />;
	}
}
