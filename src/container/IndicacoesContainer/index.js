// @flow
import * as React from "react";
import Indicacoes from "../../stories/screens/Indicacoes";
export interface Props {
	navigation: any,
}
export interface State {}
export default class IndicacoesContainer extends React.Component<Props, State> {
	render() {
		return <Indicacoes navigation={this.props.navigation} />;
	}
}
