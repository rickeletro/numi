import * as React from "react";
import { Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Left,
	Body,
	Right,
	List,
	ListItem,
	Spinner
} from "native-base";
import styles from "./styles";

class MenuIntroducao extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			datas: [
				{
					title: 'Semana 1',
					id: '25'
				},
				{
					title: 'Semana 2',
					id: '26'
				},
				{
					title: 'Semana 3',
					id: '27'
				},
				{
					title: 'Semana 4',
					id: '28'
				},
				{
					title: 'Semana 5',
					id: '29'
				},
				{
					title: 'Semana 6',
					id: '30'
				},
				{
					title: 'Semana 7',
					id: '31'
				},
				{
					title: 'Semana 8',
					id: '32'
				},

			]
		}
	}
	
	render() {
		// const param = this.props.navigation.state.params;
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body >
						<Title>Cronograma do curso</Title>
					</Body>
					{/* <Right /> */}
				</Header>

				<Content padder>
					{this.state.datas ?
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<ListItem button
									onPress={() => this.props.navigation.navigate('Introducao', data)}
								>
									<Left>
										<Text>{data.title}</Text>
									</Left>
									<Right>
										<Image
											source={require('../../../../assets/right-arrow-forward.png')}
										/>
									</Right>
								</ListItem>
							}
						/>
						:
						<Spinner style={{ marginTop: 80 }} color='orange' />
					}
				</Content>

			</Container>
		);
	}
}

export default MenuIntroducao;
