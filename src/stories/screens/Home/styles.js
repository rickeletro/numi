const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
    marginBottom: 30
  },
  logo: {
    position: "absolute",
    left: Platform.OS === "android" ? 45 : 90,
    top: Platform.OS === "android" ? 15 : 25,
    width: 186,
    height: 180
  },
  text: {
    color: "#778ca3",
    bottom: 6,
    marginTop: 5
  },
  iconEye: {
    width: 50,
    height: 50,
    tintColor: 'rgba(f,f,f,f.2)',
  },
};
