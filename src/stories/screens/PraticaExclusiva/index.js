import * as React from "react";
import { Image, WebView, AsyncStorage } from "react-native";
import moment from "moment";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Card,
	CardItem,
	Text,
	Left,
	Body,
	Right,
	List,
	Spinner
} from "native-base";
import styles from "./styles";

class PraticaExclusiva extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			datas: ''
		}

		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}
	fetchPosts = async () => {
		fetch('https://app.nucleonumi.com.br/wp-json/wp/v2/cro_praticas_formais?controle_de_acesso=24')
			.then(response => {
				return response.json();
			})
			.then(resp => {
				this.setState({
					datas: resp
				});
			});
	}
	componentDidMount() {
		this.fetchPosts();
	}
	render() {
		return (

			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body>
						<Title>Práticas exclusivas</Title>
					</Body>
					{/* <Right /> */}
				</Header>

				<Content padder>
					{this.state.datas ?
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<Card style={{ flex: 0 }}>
									<CardItem>
										<Left>
											<Body>
												<Text>{data.title.rendered}</Text>
												<Text note>{moment(data.date).utc().format("DD-MM-YYYY")}</Text>
											</Body>
										</Left>
									</CardItem>

									<CardItem bordered>
										<Body>
											<Text>
												{data.content.rendered.stripHTML()}
											</Text>
										</Body>
									</CardItem>
									{data.acf.material_video ?
										<CardItem cardBody>
											<WebView
												style={{
													width: null,
													height: 200,
													flex: 1
												}}
												javaScriptEnabled={true}
												domStorageEnabled={true}
												source={{ uri: data.acf.material_video }}
											/>
										</CardItem>
										: <Text />
									}
									{data.acf.audio ?
										<CardItem style={{ backgroundColor: '#ccc' }}>
											<Left>
												<Button transparent textStyle={{ color: '#87838B' }} onPress={() => this.props.navigation.navigate('NovoPratica', {
													onGoBack: () => this.fetchPosts(), data
												})}>
													<Image
														source={require('../../../../assets/create.png')}
													/>
													<Text>Praticar</Text>
												</Button>
											</Left>
											<Body>

											</Body>
											<Right>
												<Button transparent textStyle={{ color: '#87838B' }} onPress={() => this.props.navigation.navigate('ViewAudio', data)} >
													<Image
														source={require('../../../../assets/musical.png')}
													/>
													<Text>Audio</Text>
												</Button>
											</Right>
										</CardItem>
										:
										<CardItem style={{ backgroundColor: '#ccc' }}>
											<Left>
												<Button transparent textStyle={{ color: '#87838B' }} onPress={() => this.props.navigation.navigate('NovoPratica', {
													onGoBack: () => this.fetchPosts(), data
												})}>
													<Image
														source={require('../../../../assets/create.png')}
													/>
													<Text>Praticar</Text>
												</Button>
											</Left>
											<Body>

											</Body>
											<Right>
												<Button transparent textStyle={{ color: '#fff' }} >

												</Button>
											</Right>
										</CardItem>
									}
								</Card>
							}
						/>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>
			</Container>
		);
	}
}

export default PraticaExclusiva;
