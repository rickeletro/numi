import * as React from "react";
import { Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Left,
	Body,
	Right,
	List,
	ListItem,
	Spinner
} from "native-base";
import styles from "./styles";

class Depoimentos extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			datas: ''
		}
	}

	componentDidMount() {
		fetch('https://app.nucleonumi.com.br/wp-json/wp/v2/depoimento')
			.then(response => {
				return response.json();
			})
			.then(resp => {
				this.setState({
					datas: resp
				});
			});
	}
	render() {
		// const param = this.props.navigation.state.params;
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ alignItems: "center" }}>
						<Title>Depoimentos</Title>
					</Body>
					<Right />
				</Header>

				<Content padder>
					{this.state.datas ?
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<ListItem button
									onPress={() => this.props.navigation.navigate('ViewDepoimentos', data)}
								>
									<Left>
										<Text>{data.title.rendered}</Text>
									</Left>
									<Right>
										<Image
											source={require('../../../../assets/right-arrow-forward.png')}
										/>
									</Right>
								</ListItem>
							}
						/>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>

			</Container>
		);
	}
}

export default Depoimentos;
