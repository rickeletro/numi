import * as React from "react";
import { AsyncStorage, Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Textarea,
	Left,
	Body,
	Right,
	Form,
	Input,
	Label,
	Item
} from "native-base";
import styles from "./styles";
// export interface Props {
// 	navigation: any;
// }
// export interface State { }
var datas = [];
class EditPratica extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: this.props.navigation.state.params.data.id,
			title: this.props.navigation.state.params.data.title.rendered,
			text: this.props.navigation.state.params.data.content.rendered
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}
	alterTitle = (value) => {
		this.setState({
			title: value
		})
	}
	alterText = (value) => {
		this.setState({
			text: value
		})
	}
	fetchPosts = async () => {
		try {
			var userToken = await AsyncStorage.getItem('access_token');
			let response = await fetch(
				'http://app.nucleonumi.com.br/wp-json/wp/v2/cro_praticas_formais/' + this.state.id, {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + userToken,
					},
					body: JSON.stringify({
						title: this.state.title,
						content: this.state.text,
					})
				});

			datas = await response.json();
			if (datas) {
				this.props.navigation.state.params.onGoBack();
				this.props.navigation.goBack()
			}
		} catch (error) {
			console.error(error);
		}
	}
	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ alignItems: "center" }}>
						<Title>Editar prática</Title>
					</Body>
					<Right />
				</Header>
				<Content padder>
					<Form>
						<Label style={styles.label}>Titulo</Label>
						<Item regular>
							<Input value={this.state.title} onChangeText={this.alterTitle} />
						</Item>
						<Label style={styles.label}>Texto</Label>
						<Textarea rowSpan={5} bordered placeholder="Digite um texto" onChangeText={this.alterText} value={this.state.text.stripHTML()} />
						<Button block style={{ flex: 1, marginTop: 25 }} onPress={this.fetchPosts}><Text> Salvar </Text></Button>
					</Form>
				</Content>
			</Container>
		);
	}
}
export default EditPratica;
