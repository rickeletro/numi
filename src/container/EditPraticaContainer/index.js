// @flow
import * as React from "react";
import EditPratica from "../../stories/screens/EditPratica";
export interface Props {
	navigation: any,
}
export interface State {}
export default class EditPraticaContainer extends React.Component<Props, State> {
	render() {
		return <EditPratica navigation={this.props.navigation} />;
	}
}
