// @flow
import * as React from "react";
import ViewForum from "../../stories/screens/ViewForum";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ViewForumContainer extends React.Component<Props, State> {
	render() {
		return <ViewForum navigation={this.props.navigation} />;
	}
}
