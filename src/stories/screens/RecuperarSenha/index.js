import * as React from "react";
import { Image, WebView } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Left,
	Body,
	Right,
	Spinner
} from "native-base";

import styles from "./styles";
class RecuperarSenha extends React.Component {
	constructor(props) {
		super(props)
	}
	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Recuperar Senha</Title>
					</Body>
					<Right />
				</Header>

				<Content padder>
					<WebView
						automaticallyAdjustContentInsets={false}
						startInLoadingState={true}
						scalesPageToFit
						renderLoading={() => <Spinner style={{ marginTop: 40 }} color='orange' />}
						contentInset={{ top: 0, right: 0, bottom: 0, left: 0 }}
						style={{ height: 650 }}
						source={{ uri: 'https://app.nucleonumi.com.br/minha-conta/lost-password/' }}
						javaScriptEnabled={true}
					/>
				</Content>

			</Container>
		);
	}
}

export default RecuperarSenha;