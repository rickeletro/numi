import * as React from "react";
import { AsyncStorage, ScrollView, Image } from "react-native";
import moment from "moment";
import {
	Container,
	Header,
	Title,
	Content,
	Card,
	CardItem,
	Button,
	Text,
	Textarea,
	Left,
	List,
	View,
	Body,
	Right,
	Form,
	Label,
	Thumbnail
} from "native-base";
import styles from "./styles";

class ViewDepoimentos extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			text: '',
			datas: [],
			stateOnOff: false,
			buttonPress: false,
			img: ''
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	// SALVA UM NOVO COMENTARIO
	alterTitle = (value) => {
		this.setState({
			title: value
		})
	}
	alterText = (value) => {
		this.setState({
			text: value
		})
	}
	saveComment = async () => {
		try {
			var userToken = await AsyncStorage.getItem('access_token');
			var author_email = await AsyncStorage.getItem('author_email');
			var author_name = await AsyncStorage.getItem('author_name');
			let response = await fetch(
				'https://app.nucleonumi.com.br/wp-json/wp/v2/comments?post=' + this.props.navigation.state.params.id, {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + userToken,
					},
					body: JSON.stringify({
						// title: this.state.title,
						content: this.state.text,
						author_name: author_name,
						author_email: author_email
					})
				});
			var respSave = await response;

			if (respSave.status == 201 || respSave.status == 200) {
				this.fetchPosts();
			} else {
				alert("Detectado comentário repetido; parece que você já disse isso!")
			}
		} catch (error) {
			console.error(error);
		}
	}
	//END

	// LISTA COMMENTS REFERENTE AO CURSO
	fetchPosts = async () => {
		try {
			let response = await fetch(
				'https://app.nucleonumi.com.br/wp-json/wp/v2/comments/?post=' + this.props.navigation.state.params.id
			);
			let resp = await response.json();
			this.setState({
				datas: resp
			})

		} catch (error) {
			console.error(error);
		}
	}

	componentDidMount() {
		this.fetchPosts();
	}

	// END
	// stripHTML = () => { return this.replace(/<.*?>/g, ''); }

	render() {

		const formComment = (
			<Content padder>
				<Form>
					<Label style={styles.label}>Depoimento:</Label>
					<Textarea rowSpan={5} bordered placeholder="Deixe seu depoimento" onChangeText={this.alterText} value={this.state.text} />
					<Button block style={{ flex: 1, marginTop: 25 }} onPress={this.saveComment}><Text> Enviar </Text></Button>
				</Form>
			</Content>
		)

		const element = <View style={styles.view1}></View>;

		const cardInit = (
			<Content>
				<Card style={{ flex: 0 }}>
					<CardItem>
						<Left>
							<Body>
								<Text>{this.props.navigation.state.params.title.rendered}</Text>
								<Text note>{moment(this.props.navigation.state.params.date).utc().format("DD-MM-YYYY")}</Text>
							</Body>
						</Left>
					</CardItem>
					<CardItem>
						<Body>
							<Text>
								{this.props.navigation.state.params.content.rendered.stripHTML()}
							</Text>
						</Body>
					</CardItem>
				</Card>
			</Content>
		)

		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ alignItems: "center" }}>
						<Title>{this.props.navigation.state.params.title.rendered}</Title>
					</Body>
					<Right />
				</Header>
				<ScrollView>
					{cardInit}
					{this.state.stateOnOff === false ? formComment : element}
					<Content padder>
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<Card style={{ flex: 0 }}>
									<CardItem>
										<Left>
											<Thumbnail source={{ uri: data.author_avatar_urls['48'] }} />
											<Body>
												<Text>{data.author_name}</Text>
												<Text note>{moment(data.date).utc().format("DD-MM-YYYY")}</Text>
											</Body>
										</Left>
									</CardItem>
									<CardItem>
										<Body>

											<Text>
												{data.content.rendered.stripHTML()}
											</Text>
										</Body>
									</CardItem>
								</Card>
							}
						/>
					</Content>
				</ScrollView>
			</Container>
		);
	}
}
export default ViewDepoimentos;
