import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import { StyleSheet, View, Text, Linking } from 'react-native';
import { Button } from 'native-base';

export default class SignupSection extends Component {
  static navigationOptions = {

  }
  constructor(props) {
    super(props)
    this.state = {
      userData: []
    }
    this._onResetPass = this._onResetPass.bind(this);
  }
  _onResetPass() {
    this.props.navigation.navigate('Mindfulness');
  }
  render() {
    return (
      <View style={styles.container}> 
        <Button transparent
          onPress={() => { alert('Entre en contato com o nucleo numi e solicite seu cadastro.')
          }}>
          <Text style={styles.text}>Criar conta</Text>
        </Button >
        <Button transparent
          onPress={() => {this._onResetPass() // Linking.openURL("https://app.nucleonumi.com.br/minha-conta/lost-password/");
          }}>
          <Text style={styles.text}>Esqueceu a senha?</Text>
        </Button >
      </View>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // top: 20,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
});
