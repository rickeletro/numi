export default {
  container: {
    backgroundColor: "#eee"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  mb: {
    marginBottom: 15
  },
  label: {
    color: '#000',
    fontSize: 16,
    marginTop: 10,
  }
};
