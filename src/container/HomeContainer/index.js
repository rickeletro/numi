// @flow
import * as React from "react";
import Home from "../../stories/screens/Home";
export interface Props {
	navigation: any,
}
export interface State {}
export default class HomeContainer extends React.Component<Props, State> {
	render() {
		return <Home navigation={this.props.navigation} />;
	}
}
