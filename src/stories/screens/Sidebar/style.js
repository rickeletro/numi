// const React = require("react-native");
// const { Platform, Dimensions } = React;
import { Platform, Dimensions } from "react-native";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 3.5,
    width: null,
    position: "relative",
    marginBottom: 0
  },
  drawerImage: {
    position: "absolute",
    // left: Platform.OS === "android" ? deviceWidth / 5 : deviceWidth / 6,
    // top: Platform.OS === "android" ? deviceHeight / 24 : deviceHeight / 25,
    width: 100 + '%',
    height: 200,
    resizeMode: "cover"
  },
  // drawerImage: {
  //   position: "absolute",
  //   left: Platform.OS === "android" ? deviceWidth / 5 : deviceWidth / 6,
  //   top: Platform.OS === "android" ? deviceHeight / 24 : deviceHeight / 25,
  //   width: 146,
  //   height: 142,
  //   resizeMode: "cover"
  // },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20,
    color: "#fff"
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  }
};
