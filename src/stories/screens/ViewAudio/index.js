import * as React from "react";
import { WebView, Image } from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Card,
    CardItem,
    Text,
    Left,
    Body,
    Right
} from "native-base";
import styles from "./styles";
class viewAudio extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            audio: this.props.navigation.state.params.acf.audio,
            title: this.props.navigation.state.params.title.rendered,
			text: this.props.navigation.state.params.content.rendered
		}
    }
    render() {
        // const param = this.props.navigation.state.params;
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Image
                                source={require('../../../../assets/left-arrow.png')}
                            />
                        </Button>
                    </Left>

                    <Body style={{ alignItems: "center" }}>
                        <Title>Audios</Title>
                    </Body>

                    <Right />
                </Header>

                <Content padder>
                    <Card style={styles.mb}>
                        <CardItem cardBody>
                            <WebView
                                style={{
                                    width: null,
                                    height: 200,
                                    flex: 1
                                }}
                                source={{ uri: this.state.audio }}
                            />
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Body>
                                    <Text>{this.state.title}</Text>
                                    <Text note>{this.state.text.stripHTML()}</Text>
                                </Body>
                            </Left>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default viewAudio;
