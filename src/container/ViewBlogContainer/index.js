// @flow
import * as React from "react";
import ViewBlog from "../../stories/screens/ViewBlog";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ViewBlogContainer extends React.Component<Props, State> {
	render() {
		return <ViewBlog navigation={this.props.navigation} />;
	}
}
