// @flow
import * as React from "react";
import Cursos from "../../stories/screens/Cursos";
export interface Props {
	navigation: any,
}
export interface State {}
export default class CursosContainer extends React.Component<Props, State> {
	render() {
		return <Cursos navigation={this.props.navigation} />;
	}
}
