// @flow
import * as React from "react";
import Audio from "../../stories/screens/Audio";
export default class AudioContainer extends React.Component {
	render() {
		return <Audio navigation={this.props.navigation} />;
	}
}
