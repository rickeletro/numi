// @flow
import * as React from "react";
import Introducao from "../../stories/screens/Introducao";
export interface Props {
	navigation: any,
}
export interface State {}
export default class IntroducaoContainer extends React.Component<Props, State> {
	render() {
		return <Introducao navigation={this.props.navigation} />;
	}
}
