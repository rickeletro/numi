// @flow
import * as React from "react";
import Lembrete from "../../stories/screens/Lembrete";
export interface Props {
	navigation: any,
}
export interface State {}
export default class LembreteContainer extends React.Component<Props, State> {
	render() {
		return <Lembrete navigation={this.props.navigation} />;
	}
}
