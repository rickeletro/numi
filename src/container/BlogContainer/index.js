// @flow
import * as React from "react";
import Blog from "../../stories/screens/Blog";
export interface Props {
	navigation: any,
}
export interface State {}
export default class BlogContainer extends React.Component<Props, State> {
	render() {
		return <Blog navigation={this.props.navigation} />;
	}
}
