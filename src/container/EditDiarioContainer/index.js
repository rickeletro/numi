// @flow
import * as React from "react";
import EditDiario from "../../stories/screens/EditDiario";
export interface Props {
	navigation: any,
}
export interface State {}
export default class EditDiarioContainer extends React.Component<Props, State> {
	render() {
		return <EditDiario navigation={this.props.navigation} />;
	}
}
