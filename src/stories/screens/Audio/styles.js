export default {
  container: {
    backgroundColor: "#ccc"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  mb: {
    marginBottom: 15
  }
};
