export default {
  container: {
    backgroundColor: "#FFF"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  mb: {
    marginBottom: 15
  },
  label: {
    color: '#ccc',
    fontSize: 16,
    marginTop: 10,
  }
};
