// @flow
import * as React from "react";
import Obstaculos from "../../stories/screens/Obstaculos";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ObstaculosContainer extends React.Component<Props, State> {
	render() {
		return <Obstaculos navigation={this.props.navigation} />;
	}
}
