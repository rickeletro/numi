// @flow
import * as React from "react";
import RecuperarSenha from "../../stories/screens/RecuperarSenha";
export interface Props {
	navigation: any,
}
export interface State {}
export default class RecuperarSenhaContainer extends React.Component<Props, State> {
	render() {
		return <RecuperarSenha navigation={this.props.navigation} />;
	}
}
