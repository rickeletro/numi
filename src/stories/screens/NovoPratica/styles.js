
export default {
  text: {
    color: "#778ca3",
    bottom: 6,
    marginTop: 5
  },
  label: {
    color: '#ccc',
    fontSize: 16,
    marginTop: 10,
  },
  view: {
    height: 300,
    backgroundColor: "#EDA04C",
    flex: 1,
    paddingTop: 15,
    paddingBottom: 15,
  },
  cron: {
    fontSize: 50,
    color: "#fff"
  }
};
