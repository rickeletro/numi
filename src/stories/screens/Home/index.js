import React, { Component } from "react";
import { Image, TouchableOpacity } from "react-native";
import { Container, Button, Header, Title, Left, Body, Right } from "native-base";

class Home extends Component {

  render() {
    const cardButton = {
      backgroundColor: "#EDA04C",
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: { height: 5, width: 0 },
      elevation: 1,
      shadowOpacity: 1
    }
    return (
      <Container>

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
              <Image
                source={require('../../../../assets/menu.png')}
              />
            </Button>
          </Left>
          <Body>
            <Title></Title>
          </Body>
          <Right />
        </Header>

        <TouchableOpacity onPress={() => this.props.navigation.navigate("PraticaGratuita")} style={cardButton}>
          <Image
            source={require('../../../../assets/material.png')}
          />
          <Title style={{ paddingTop: 22 }}>Práticas Gratuitas</Title>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.props.navigation.navigate("PraticaExclusiva")} style={cardButton}>
          <Image
            source={require('../../../../assets/material.png')}
          />
          <Title style={{ paddingTop: 22 }}>Práticas Exclusivas</Title>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.props.navigation.navigate("Blog")} style={cardButton}>
          <Image
            source={require('../../../../assets/blog.png')}
          />
          <Title style={{ paddingTop: 22 }}>Blog</Title>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => this.props.navigation.navigate("Cursos")} style={cardButton}>
          <Image
            source={require('../../../../assets/home.png')}
          />
          <Title style={{ paddingTop: 22 }}>Cursos e Eventos</Title>
        </TouchableOpacity>

      </Container>
    )
  }
}

export default Home;
