import * as React from "react";
import { Image, WebView } from "react-native";
import moment from "moment";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Card,
	CardItem,
	Text,
	Left,
	Body,
	Right,
	List
} from "native-base";
import styles from "./styles";

class Indicacoes extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			datas: []
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}
	fetchPosts = async () => {
		fetch('http://app.nucleonumi.com.br/wp-json/wp/v2/indicacoes')
			.then(response => {
				return response.json();
			})
			.then(resp => {
				this.setState({
					datas: resp
				});
			});
	}
	componentDidMount() {
		this.fetchPosts();
	}
	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Indicações</Title>
					</Body>
					<Right></Right>
				</Header>

				<Content padder>
					<List
						dataArray={this.state.datas}
						renderRow={data =>
							<Card style={{ flex: 0 }}>
								<CardItem>
									<Left>
										<Body>
											<Text>{data.title.rendered}</Text>
											<Text note>{moment(data.date).utc().format("DD-MM-YYYY")}</Text>
										</Body>
									</Left>
								</CardItem>

								<CardItem bordered>
									<Body>
										{data.acf.imagem_post ?
											<Image source={{ uri: data.acf.imagem_post.sizes.medium_large }} style={{ height: 189, width: '100%', flex: 1 }} />
											: <Image source={require('../../../../assets/nuvens.png')} style={{ height: 189, width: '100%', flex: 1 }} />
										}
										<Text style={{ paddingTop: 15 }}>
											{data.content.rendered.stripHTML()}
										</Text>
									</Body>
								</CardItem>
								{data.acf.material_video ?
									<CardItem cardBody>
										<WebView
											style={{
												width: null,
												height: 200,
												flex: 1
											}}
											javaScriptEnabled={true}
											domStorageEnabled={true}
											source={{ uri: data.acf.material_video }}
										/>
									</CardItem>
									: <Text />
								}
								{data.acf.audio ?
									<CardItem style={{ backgroundColor: '#ccc' }}>
										<Left>
											<Button transparent textStyle={{ color: '#87838B' }} onPress={() => this.props.navigation.navigate('ViewAudio', data)} >
												<Image
													source={require('../../../../assets/musical.png')}
												/>
												<Text>Audio</Text>
											</Button>
										</Left>
										<Body>
											<Button transparent textStyle={{ color: '#fff' }} >

											</Button>
										</Body>
										<Right>

										</Right>
									</CardItem>
									:
									<CardItem style={{ backgroundColor: '#ccc' }}>
										<Left>
											<Button transparent textStyle={{ color: '#fff' }} >

											</Button>
										</Left>
										<Body>

										</Body>
										<Right>
											<Button transparent textStyle={{ color: '#fff' }} >

											</Button>
										</Right>
									</CardItem>
								}
							</Card>
						}
					/>
				</Content>
			</Container>
		);
	}
}

export default Indicacoes;
