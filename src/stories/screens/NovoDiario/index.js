import * as React from "react";
import { AsyncStorage, Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Textarea,
	Left,
	Body,
	Right,
	Form,
	Input,
	Label,
	Item,
} from "native-base";
import styles from "./styles";
// export interface Props {
// 	navigation: any;
// }
// export interface State { }
var datas = [];
class NovoDiario extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			text: '',
			// user: this.props.navigation.state.params.userid
		}
	}
	alterTitle = (value) => {
		this.setState({
			title: value
		})
	}
	alterText = (value) => {
		this.setState({
			text: value
		})
	}
	fetchPosts = async () => {
		try {
			var userToken = await AsyncStorage.getItem('access_token');
			let response = await fetch(
				'https://app.nucleonumi.com.br/wp-json/wp/v2/diario_de_praticas/', {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + userToken,
					},
					body: JSON.stringify({
						title: this.state.title,
						content: this.state.text,
						status: 'publish'
					})
				});
			datas = await response.json();
			if (datas) {
				this.props.navigation.state.params.onGoBack();
				this.props.navigation.goBack()
			}
		} catch (error) {
			console.error(error);
		}
	}
	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ alignItems: "center" }}>
						<Title>Novo Diário</Title>
					</Body>
					<Right />
				</Header>
				<Content padder>
					<Form>
						<Label style={styles.label}>Titulo</Label>
						<Item regular>
							<Input value={this.state.title} onChangeText={this.alterTitle} />
						</Item>
						<Label style={styles.label}>Texto</Label>
						<Textarea rowSpan={5} bordered placeholder="Digite um texto" onChangeText={this.alterText} value={this.state.text} />
						<Button block style={{ flex: 1, marginTop: 25 }} onPress={this.fetchPosts}><Text> Salvar </Text></Button>
					</Form>
				</Content>
			</Container>
		);
	}
}
export default NovoDiario;
