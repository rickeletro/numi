import * as React from "react";
import { Image } from "react-native";
// import { decode as atob } from "base-64";
import moment from "moment";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Card,
	CardItem,
	Left,
	Body,
	Right,
	List,
	Spinner
} from "native-base";
import styles from "./styles";
class Obstaculos extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			datas: ''
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}


	fetchPosts = async () => {
		try {
			let response = await fetch(
				'https://app.nucleonumi.com.br/wp-json/wp/v2/obstaculos_a_pratica'
			);
			let resp = await response.json();
			await this.setState({
				datas: resp
			})
		} catch (error) {
			console.error(error);
		}
	}

	componentWillMount() {
		this.fetchPosts();
	}

	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Obstáculos à prática</Title>
					</Body>
					<Right></Right>

				</Header>
				<Content padder>
					{this.state.datas ?
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<Card style={{ flex: 0 }}>
									<CardItem>
										<Left>
											<Body>
												<Text>{data.title.rendered}</Text>
												<Text note>{moment(data.date).utc().format("DD-MM-YYYY")}</Text>
											</Body>
										</Left>
									</CardItem>

									<CardItem bordered>
										<Body>
											<Text>
												{data.content.rendered.stripHTML()}
											</Text>
										</Body>
									</CardItem>
								</Card>
							}
						/>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>

			</Container>
		);
	}
}

export default Obstaculos;
