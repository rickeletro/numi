import * as React from "react";
import { View, AsyncStorage, ScrollView, Image, Vibration } from "react-native";
import { Container, Icon, Content, Button, Picker, Text, Header, Title, Left, Body, Right, Form, Item, Label, Input, Textarea } from "native-base";
import { Player } from 'react-native-audio-toolkit';

import styles from "./styles";
import qs from "query-string";
class NovoPratica extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.navigation.state.params.data.title.rendered,
      text: '',
      cronometro: 0,
      cronText: '00:00:00',
      cronTemp: '',
      cronTempHor: '00',
      cronTempMin: '00',
      stateOnOff: false,
      buttonPress: false,
      active: 'true'
    }
  }
  alterTitle = (value) => {
    this.setState({
      title: value
    })
  }
  alterText = (value) => {
    this.setState({
      text: value
    })
  }
  alterCrono = () => {
    s = this.state.cronometro
    function duas_casas(numero) {
      if (numero <= 9) {
        numero = "0" + numero;
      }
      return numero;
    }
    hora = duas_casas(Math.floor(s / 3600));
    minuto = duas_casas(Math.floor((s % 3600) / 60));
    segundo = duas_casas((s % 3600) % 60);

    formatado = hora + ":" + minuto + ":" + segundo;
    this.setState({
      cronText: formatado
    })
    this.setState({
      cronometro: this.state.cronometro - 1
    })
    if (this.state.cronometro < 0) {
      this.stopCron();
      Vibration.vibrate(500, false);
      new Player('bell.mp3').play();
    }
  }

  timeToSeconds = () => {
    let hora = parseInt(this.state.cronTempHor);
    let minuto = parseInt(this.state.cronTempMin);
    let qtdeSegundos = (hora * 3600) + (minuto * 60);
    this.setState({
      cronometro: qtdeSegundos
    })
  }

  playCron = async () => {
    new Player('bell.mp3').play();
    this.setState({
      buttonPress: true, // comuta botao buttonPlay para buttonStop
      stateOnOff: false
    })
    this.timerID = setInterval(
      () => this.alterCrono(),
      1000
    );
    this.timeToSeconds();
    // tempo que vai ser salvo como praticado
    let horaFinal = this.state.cronTempHor + ":" + this.state.cronTempMin + ":00";
    this.setState({
      cronTemp: horaFinal
    })
  }

  stopCron = () => {
    clearInterval(this.timerID);
    this.setState({
      stateOnOff: true,
      buttonPress: false // comuta botao buttonStop para buttonPlay
    })
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  fetchPosts = async () => {
    try {
      var userToken = await AsyncStorage.getItem('access_token');
      let response = await fetch(
        'https://app.nucleonumi.com.br/wp-json/wp/v2/diario_de_praticas/', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + userToken,
          },
          body: JSON.stringify({
            title: this.state.title,
            content: this.state.text,
            status: 'publish',
          })
        });
      (response);
      datas = await response.json();
      if (datas) {
        let response2 = await fetch(
          'https://app.nucleonumi.com.br/wp-json/acf/v3/diario_de_praticas/' + datas.id, {
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Bearer ' + userToken,
            },
            body: qs.stringify({ 'fields[crono_pratica]': this.state.cronTemp })
          });
        let acfResp = await response2.json();
        if (acfResp) {
          this.props.navigation.state.params.onGoBack();
          this.props.navigation.goBack()
        }
      }
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    const element = (
      <View>
        <Content padder>
          <Form>
            <Label style={styles.label}>Pratica</Label>
            <Item regular>
              <Input value={this.state.title} />
            </Item>
            <Label style={styles.label}>Qual foi sua experiência:</Label>
            <Textarea rowSpan={5} bordered placeholder="Deixe um comentario sobre sua experiência nesta pratica." onChangeText={this.alterText} value={this.state.text} />
            <Button block style={{ flex: 1, marginTop: 25 }} onPress={this.fetchPosts}><Text> Salvar </Text></Button>
          </Form>
        </Content>
      </View>
    );
    const element2 = <View></View>;
    const buttonPlay = <Button block onPress={this.playCron}><Text> Iniciar </Text></Button>;
    const buttonStop = <Button block onPress={this.stopCron}><Text> Parar </Text></Button>
    return (
      <Container>

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Image
                source={require('../../../../assets/left-arrow.png')}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, paddingLeft: 33, }}>
            <Title>Cronômetro de Praticas</Title>
          </Body>
          <Right />
        </Header>

        <ScrollView style={this.state.stateOnOff === false ? { backgroundColor: "#EDA04C" } : { backgroundColor: "#fff" }}>

          <View style={styles.view}>
            <Body style={{ alignItems: "center" }}>
              <Text>Quanto tempo deseja praticar?</Text>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ width: 115, marginRight: 15 }}>

                  <Picker
                    selectedValue={this.state.cronTempHor}
                    style={{ color: "#fff" }}
                    onValueChange={(itemValue, itemIndex) => this.setState({ cronTempHor: itemValue })}>
                    <Picker.Item label="00" value="00" />
                    <Picker.Item label="1 hora" value="01" />
                    <Picker.Item label="2 horas" value="02" />
                    <Picker.Item label="3 horas" value="03" />
                    <Picker.Item label="4 horas" value="04" />
                    <Picker.Item label="5 horas" value="06" />
                    <Picker.Item label="6 horas" value="06" />
                    <Picker.Item label="7 horas" value="07" />
                    <Picker.Item label="8 horas" value="08" />
                  </Picker>
                </View>
                <View style={{ width: 100 }}>
                  <Picker
                    selectedValue={this.state.cronTempMin}
                    style={{ color: "#fff" }}
                    onValueChange={(itemValue, itemIndex) => this.setState({ cronTempMin: itemValue })}>
                    <Picker.Item label="00" value="00" />
                    <Picker.Item label="1 min" value="01" />
                    <Picker.Item label="5 min" value="05" />
                    <Picker.Item label="10 min" value="10" />
                    <Picker.Item label="15 min" value="15" />
                    <Picker.Item label="20 min" value="20" />
                    <Picker.Item label="25 min" value="25" />
                    <Picker.Item label="30 min" value="30" />
                    <Picker.Item label="35 min" value="35" />
                    <Picker.Item label="40 min" value="40" />
                    <Picker.Item label="45 min" value="45" />
                    <Picker.Item label="50 min" value="50" />
                    <Picker.Item label="55 min" value="55" />
                  </Picker>
                  {/* <Item regular >
                    <Input placeholder='mm' keyboardType={'number-pad'} maxLength={2} value={this.state.cronTempMin} onChangeText={this.alterMin} />
                  </Item> */}
                </View>
              </View>

              <Text style={styles.cron}>{this.state.cronText}</Text>

              {this.state.buttonPress === false ? buttonPlay : buttonStop}

            </Body>
          </View>

          {this.state.stateOnOff === true ? element : element2}

        </ScrollView>
      </Container>
    );
  }
}

export default NovoPratica;
