import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {StyleSheet, View, Image} from 'react-native';

import logoImg from '../../../../../assets/logo_login.png';

export default class Logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={logoImg} style={styles.image} />
        {/* <Text style={styles.text}>REACT NATIVE</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 165,
    height: 160,
  },
});
