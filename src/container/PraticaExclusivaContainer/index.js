// @flow
import * as React from "react";
import PraticaExclusiva from "../../stories/screens/PraticaExclusiva";
export interface Props {
	navigation: any,
}
export interface State {}
export default class PraticaExclusivaContainer extends React.Component<Props, State> {
	render() {
		return <PraticaExclusiva navigation={this.props.navigation} />;
	}
}
