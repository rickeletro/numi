import React, { Component } from "react";
import { WebView, Image, AsyncStorage, } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Card,
	CardItem,
	Text,
	Left,
	Body,
	Right,
	List,
	Spinner
} from "native-base";
import styles from "./styles";

class Materiais extends Component {
	constructor(props) {
		super(props)
		this.state = {
			datas: []
		};
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	// verify = async () => {
	// 	const userToken = await AsyncStorage.getItem('access_token');
	// 	this.props.navigation.navigate(userToken ? 'Materiais' : 'Login');
	// };
	// componentWillMount() {
	// 	this.verify();
	// }

	componentDidMount() {
		fetch('https://app.nucleonumi.com.br/wp-json/wp/v2/material_de_curso?tipo_de_material=20')
			.then(response => {
				return response.json();
			})
			.then(resp => {
				this.setState({
					datas: resp
				});
			});
	}
	render() {
		// const param = this.props.navigation.state.params;
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Materiais de Curso</Title>
					</Body>

					<Right />
				</Header>

				<Content padder>
					{this.state.datas ?
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<Card style={styles.mb}>
									<CardItem cardBody>
										<WebView
											style={{
												width: null,
												height: 200,
												flex: 1
											}}
											javaScriptEnabled={true}
											domStorageEnabled={true}
											source={{ uri: data.acf.material_video }}
										/>
									</CardItem>
									<CardItem>
										<Left>
											<Body>
												<Text>{data.title.rendered}</Text>
												<Text note>{data.content.rendered.stripHTML()}</Text>
											</Body>
										</Left>
									</CardItem>
								</Card>
							}
						/>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>
			</Container>
		);
	}
}

export default Materiais;
