// @flow
import * as React from "react";
import Cronometro from "../../stories/screens/Cronometro";
export interface Props {
	navigation: any,
}
export interface State {}
export default class CronometroContainer extends React.Component<Props, State> {
	render() {
		return <Cronometro navigation={this.props.navigation} />;
	}
}
