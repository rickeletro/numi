// @flow
import * as React from "react";
import ViewAudio from "../../stories/screens/ViewAudio";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ViewAudioContainer extends React.Component<Props, State> {
	render() {
		return <ViewAudio navigation={this.props.navigation} />;
	}
}
