import * as React from "react";
import { AsyncStorage, Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Textarea,
	Left,
	Body,
	Right,
	Form,
	Input,
	Label,
	Item
} from "native-base";
import styles from "./styles";
// var datas = [];
class EditDiario extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: this.props.navigation.state.params.data.id,
			title: this.props.navigation.state.params.data.title.rendered,
			text: this.props.navigation.state.params.data.content.rendered,
			time: this.props.navigation.state.params.data.acf.crono_pratica
		}

		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	alterTitle = (value) => {
		this.setState({
			title: value
		})
	}
	alterText = (value) => {
		this.setState({
			text: value
		})
	}
	fetchPosts = async () => {
		try {
			var userToken = await AsyncStorage.getItem('access_token');
			let response = await fetch(
				'http://app.nucleonumi.com.br/wp-json/wp/v2/diario_de_praticas/' + this.state.id, {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + userToken,
					},
					body: JSON.stringify({
						title: this.state.title,
						content: this.state.text,
					})
				});

			var datas = await response.json();
			if (datas) {
				this.props.navigation.state.params.onGoBack();
				this.props.navigation.goBack()
			}
		} catch (error) {
			console.error(error);
		}
	}
	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>

					<Body style={{ alignItems: "center" }}>
						<Title>Editar Diário</Title>
					</Body>
					<Right />
				</Header>
				<Content padder>
					<Form>
						<Label style={styles.label}>Que pratica Fiz.</Label>
						<Item regular>
							<Input value={this.state.title} disabled />
						</Item>
						<Label style={styles.label}>Tempo</Label>
						<Item regular>
							<Input value={this.state.time} disabled />
						</Item>
						<Label style={styles.label}>Qual foi sua experiência?</Label>
						<Textarea rowSpan={5} bordered placeholder="Faç um breve descriçao da sua experiencia." onChangeText={this.alterText} value={this.state.text.stripHTML()} />
						<Button block style={{ flex: 1, marginTop: 25 }} onPress={this.fetchPosts}><Text> Salvar </Text></Button>
					</Form>
				</Content>
			</Container>
		);
	}
}
export default EditDiario;
