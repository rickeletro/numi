import * as React from "react";
import { View, ScrollView, Image, Vibration } from "react-native";
import { Button, Picker, Text, Left, Body, ListItem } from "native-base";
import { Player } from 'react-native-audio-toolkit';
import BackgroundTimer from 'react-native-background-timer';
const alerta = require("../../../../assets/clock.png");

import styles from "./styles";

class Lembrete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cronometro: 0,
      cronText: '00:00:00',
      cronTemp: '',
      cronTempHor: '00',
      cronTempMin: '00',
      stateOnOff: false,
      buttonPress: false,
      buttonMenu: false,
      menu: {
        name: "Alarme",
        icon: alerta,
        bg: "#C5F442"
      },
    }
  }
  alterCrono = () => {
    this.setState({
      cronometro: this.state.cronometro - 1
    })
    if (this.state.cronometro < 0) {
      // this.stopCron();
      this.timeToSeconds();
      Vibration.vibrate(500, false);
      new Player('bell.mp3').play();
    }
  }

  timeToSeconds = () => {
    let hora = parseInt(this.state.cronTempHor);
    let minuto = parseInt(this.state.cronTempMin);
    let qtdeSegundos = (hora * 3600) + (minuto * 60);
    this.setState({
      cronometro: qtdeSegundos
    })
  }

  playCron = async () => {
    new Player('bell.mp3').play();
    this.setState({
      buttonPress: true, // comuta botao buttonPlay para buttonStop
      stateOnOff: false
    })

    this.timerID = BackgroundTimer.setInterval(() => {
      this.alterCrono();
    }, 1000);
    this.timeToSeconds();
  }

  stopCron = () => {
    // clearInterval(this.timerID);
    BackgroundTimer.clearInterval(this.timerID);
    this.setState({
      stateOnOff: true,
      buttonPress: false // comuta botao buttonStop para buttonPlay
    })
  }
  render() {
    const buttonPlay = <Button block onPress={this.playCron}><Text> Agendar </Text></Button>;
    const buttonStop = <Button block onPress={this.stopCron}><Text> Pausar </Text></Button>;
    // const alarme = (
     
    // );
    return (
      // <Container>
      <ScrollView style={styles.contentContainer} >
       <View style={styles.view}>
        <Body style={{ alignItems: "center" }}>
          <Text style={styles.text}>Com que frequência deseja receber lembrete?</Text>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ width: 115, marginRight: 15 }}>

              <Picker
                selectedValue={this.state.cronTempHor}
                style={{ color: "#fff" }}
                onValueChange={(itemValue) => this.setState({ cronTempHor: itemValue })}>
                <Picker.Item label="00" value="00" />
                <Picker.Item label="1 hora" value="01" />
                <Picker.Item label="2 horas" value="02" />
                <Picker.Item label="3 horas" value="03" />
                <Picker.Item label="4 horas" value="04" />
                <Picker.Item label="5 horas" value="06" />
                <Picker.Item label="6 horas" value="06" />
                <Picker.Item label="7 horas" value="07" />
                <Picker.Item label="8 horas" value="08" />
              </Picker>
            </View>
            <View style={{ width: 100 }}>
              <Picker
                selectedValue={this.state.cronTempMin}
                style={{ color: "#fff" }}
                onValueChange={(itemValue) => this.setState({ cronTempMin: itemValue })}>
                <Picker.Item label="00" value="00"/>
                <Picker.Item label="1 min" value="01" />
                <Picker.Item label="5 min" value="05" />
                <Picker.Item label="10 min" value="10" />
                <Picker.Item label="15 min" value="15" />
                <Picker.Item label="20 min" value="20" />
                <Picker.Item label="25 min" value="25" />
                <Picker.Item label="30 min" value="30" />
                <Picker.Item label="35 min" value="35" />
                <Picker.Item label="40 min" value="40" />
                <Picker.Item label="45 min" value="45" />
                <Picker.Item label="50 min" value="50" />
                <Picker.Item label="55 min" value="55" />
              </Picker>
            </View>
          </View>
          {this.state.buttonPress === false ? buttonPlay : buttonStop}
        </Body>
      </View>
      {/* {this.state.buttonMenu === true ? alarme : <View></View>}
      <ListItem
        button
        onPress={() => {
          this.setState({
            buttonMenu: !this.state.buttonMenu
          })
        }}
        style={{ backgroundColor: "#EDA04C" }}
      >
        <Left >
          <Image
            source={this.state.menu.icon}
          />
          <Text style={styles.text}>
            {this.state.menu.name}
          </Text>
        </Left>
      </ListItem> */}
        
      </ScrollView>
      // </Container>
    );
  }
}

export default Lembrete;
