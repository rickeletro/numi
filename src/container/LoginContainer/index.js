// @flow
import * as React from "react";
import Login from "../../stories/screens/Login";
export interface Props {
	navigation: any,
}
export interface State {}
export default class LoginContainer extends React.Component<Props, State> {
	render() {
		return <Login navigation={this.props.navigation} />;
	}
}
