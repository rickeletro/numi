// @flow
import * as React from "react";
import ViewCurso from "../../stories/screens/ViewCurso";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ViewCursoContainer extends React.Component<Props, State> {
	render() {
		return <ViewCurso navigation={this.props.navigation} />;
	}
}
