// @flow
import * as React from "react";
import Salas from "../../stories/screens/Salas";
export interface Props {
	navigation: any,
}
export interface State {}
export default class SalasContainer extends React.Component<Props, State> {
	render() {
		return <Salas navigation={this.props.navigation} />;
	}
}
