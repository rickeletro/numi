// @flow
import * as React from "react";
import NovoDiario from "../../stories/screens/NovoDiario";
export interface Props {
	navigation: any,
}
export interface State {}
export default class NovoDiarioContainer extends React.Component<Props, State> {
	render() {
		return <NovoDiario navigation={this.props.navigation} />;
	}
}
