import * as React from "react";
import { Image, AsyncStorage, ScrollView } from "react-native";
import { Text, Container, List, ListItem, Content, Left } from "native-base";
// import { NavigationActions } from "react-navigation";
import styles from "./style";
import Lembrete from "../Lembrete";
const drawerCover = require("../../../../assets/topsidebar.png");
const drawerImage = require("../../../../assets/logo.png");
const materiais = require('../../../../assets/material.png');
const audio = require('../../../../assets/musical.png');
const pratica = require("../../../../assets/clock.png");
const salas = require("../../../../assets/conversation.png");
const diario = require("../../../../assets/diary.png");
const blog = require("../../../../assets/blog.png");
const home = require("../../../../assets/home.png");
const forum = require("../../../../assets/conversation.png");
const depoimentos = require("../../../../assets/depoimento.png");
const logout = require("../../../../assets/logout.png");
const create = require("../../../../assets/create.png");

const routes = [
	{
		route: "Mindfulness",
		name: "Mindfulness",
		icon: home,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "MenuIntroducao",
		name: "Curso Introdutório em Mindfulness",
		icon: materiais,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "PraticaGratuita",
		name: "Práticas Gratuitas",
		icon: materiais,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "PraticaExclusiva",
		name: "práticas Exclusivas",
		icon: audio,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "Cronometro",
		name: "Cronômetro de Práticas",
		icon: pratica,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "Diario",
		name: "Diários de Práticas",
		icon: diario,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "Forum",
		name: "Fórum de Compartilhamento",
		icon: forum,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "Salas",
		name: "Prática Online",
		icon: salas,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "QuemSomos",
		name: "Quem Somos",
		icon: home,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "Cursos",
		name: "Cursos e Eventos",
		icon: home,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "Blog",
		name: "Blog",
		icon: blog,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "Depoimentos",
		name: "Depoimentos",
		icon: depoimentos,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "Perguntas",
		name: "Perguntas Frequentes",
		icon: salas,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "Obstaculos",
		name: "Obstáculos à Prática e Antídotos",
		icon: blog,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "Indicacoes",
		name: "Indicações",
		icon: materiais,
		bg: "#C5F442",
		access: "restrict"
	},
	{
		route: "RecuperarSenha",
		name: "Recuperar Senha",
		icon: create,
		bg: "#C5F442",
		access: ""
	},
	{
		route: "Login",
		name: "Logout",
		icon: logout,
		bg: "#C5F442",
		access: ""
	},
];

export default class Sidebar extends React.Component {

	_bootstrapAsync = async (rota) => {
		const userToken = await AsyncStorage.getItem('access_token');
		this.props.navigation.navigate(userToken ? rota : 'Login');
	};
	_routeNavigation = async (rota) => {
		if (rota === 'Login') {
			AsyncStorage.clear();
		}
		this.props.navigation.navigate(rota)
	}

	render() {
		return (
			<Container>
				<Content
					bounces={false}
					style={{ flex: 1, backgroundColor: "#EDA04C", top: -1 }}
				>
					<Image style={styles.drawerCover} source={drawerCover} />
					<Image square style={styles.drawerImage} source={drawerImage} />
					<ScrollView>
						<List
							style={{ marginTop: 20 }}
							dataArray={routes}
							renderRow={data => {
								return (
									<ListItem
										button
										onPress={() => {
											data.access === "restrict"
												? this._bootstrapAsync(data.route) // Se acesso é restrito executa a funçao para verificar se esta logado.
												: this._routeNavigation(data.route);
										}}
										style={{ backgroundColor: "#EDA04C" }}
									>
										<Left >
											<Image
												source={data.icon}
											/>
											<Text style={styles.text}>
												{data.name}
											</Text>
										</Left>
									</ListItem>
								);
							}}
						/>
						<Lembrete />
					</ScrollView>
				</Content>
			</Container>
		);
	}
}
