// @flow
import * as React from "react";
import MenuIntroducao from "../../stories/screens/MenuIntroducao";
export interface Props {
	navigation: any,
}
export interface State {}
export default class MenuIntroducaoContainer extends React.Component<Props, State> {
	render() {
		return <MenuIntroducao navigation={this.props.navigation} />;
	}
}
