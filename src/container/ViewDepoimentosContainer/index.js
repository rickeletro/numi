// @flow
import * as React from "react";
import ViewDepoimentos from "../../stories/screens/ViewDepoimentos";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ViewDepoimentosContainer extends React.Component<Props, State> {
	render() {
		return <ViewDepoimentos navigation={this.props.navigation} />;
	}
}
