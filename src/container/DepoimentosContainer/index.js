// @flow
import * as React from "react";
import Depoimentos from "../../stories/screens/Depoimentos";
export interface Props {
	navigation: any,
}
export interface State {}
export default class DepoimentosContainer extends React.Component<Props, State> {
	render() {
		return <Depoimentos navigation={this.props.navigation} />;
	}
}
