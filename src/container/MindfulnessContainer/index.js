// @flow
import * as React from "react";
import Mindfulness from "../../stories/screens/Mindfulness";
export interface Props {
	navigation: any,
}
export interface State {}
export default class MindfulnessContainer extends React.Component<Props, State> {
	render() {
		return <Mindfulness navigation={this.props.navigation} />;
	}
}
