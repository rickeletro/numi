// @flow
import * as React from "react";
import Materiais from "../../stories/screens/Materiais";
export interface Props {
	navigation: any,
}
export interface State {}
export default class MateriaisContainer extends React.Component<Props, State> {
	render() {
		return <Materiais navigation={this.props.navigation} />;
	}
}
