// @flow
import React from "react";
import { Root } from "native-base";
import { createStackNavigator, createDrawerNavigator } from "react-navigation";

import Login from "./container/LoginContainer";
import Home from "./container/HomeContainer";
import Materiais from "./container/MateriaisContainer";
import Audio from "./container/AudioContainer";
import Sidebar from "./container/SidebarContainer";
import Salas from "./container/SalasContainer";
import Diario from "./container/DiarioContainer";
import EditDiario from "./container/EditDiarioContainer";
import NovoDiario from "./container/NovoDiarioContainer";
import MenuIntroducao from "./container/MenuIntroducaoContainer";
import Introducao from "./container/IntroducaoContainer";
import PraticaGratuita from "./container/PraticaGratuitaContainer";
import PraticaExclusiva from "./container/PraticaExclusivaContainer";
import Indicacoes from "./container/IndicacoesContainer";
import Cronometro from "./container/CronometroContainer";
import NovoPratica from "./container/NovoPraticaContainer";
import EditPratica from "./container/EditPraticaContainer";
import Blog from "./container/BlogContainer";
import Forum from "./container/ForumContainer";
import ViewForum from "./container/ViewForumContainer";
import ViewAudio from "./container/ViewAudioContainer";
import Depoimentos from "./container/DepoimentosContainer";
import ViewDepoimentos from "./container/ViewDepoimentosContainer";
import ViewBlog from "./container/ViewBlogContainer";
import ViewCurso from "./container/ViewCursoContainer";
import QuemSomos from "./container/QuemSomosContainer";
import Mindfulness from "./container/MindfulnessContainer";
import RecuperarSenha from "./container/RecuperarSenhaContainer";
import Cursos from "./container/CursosContainer";
import Obstaculos from "./container/ObstaculosContainer";
import Perguntas from "./container/PerguntasContainer";
import Lembrete from './container/LembreteContainer';

const Drawer = createDrawerNavigator(
	{
		Home: { screen: Home },
	},
	{
		initialRouteName: "Home",
		contentComponent: props => <Sidebar {...props} />,
	}
);

const App = createStackNavigator(
	{
		Drawer: { screen: Drawer },
		Login: { screen: Login },
		Materiais: { screen: Materiais },
		Audio: { screen: Audio },
		Salas: { screen: Salas },
		Diario: { screen: Diario },
		MenuIntroducao: { screen: MenuIntroducao },
		EditDiario: { screen: EditDiario },
		NovoDiario: { screen: NovoDiario },
		Introducao: { screen: Introducao },
		PraticaGratuita: { screen: PraticaGratuita },
		PraticaExclusiva: { screen: PraticaExclusiva },
		Indicacoes: { screen: Indicacoes },
		Cronometro: { screen: Cronometro },
		NovoPratica: { screen: NovoPratica },
		EditPratica: { screen: EditPratica },
		Blog: { screen: Blog },
		Forum: { screen: Forum },
		ViewForum: { screen: ViewForum },
		ViewAudio: { screen: ViewAudio },
		Depoimentos: { screen: Depoimentos },
		ViewDepoimentos: { screen: ViewDepoimentos },
		ViewBlog: { screen: ViewBlog },
		ViewCurso: { screen: ViewCurso },
		Cursos: { screen: Cursos },
		Obstaculos: { screen: Obstaculos },
		QuemSomos: { screen: QuemSomos },
		Mindfulness: { screen: Mindfulness },
		RecuperarSenha: { screen: RecuperarSenha },
		Perguntas: { screen: Perguntas },
		Lembrete: { screen: Lembrete },
		// Drawer: { screen: Drawer },
	},
	{
		initialRouteName: "Drawer",
		headerMode: "none",
	}
);

export default () => (
	<Root>
		<App />
	</Root>
);
