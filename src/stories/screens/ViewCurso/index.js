import * as React from "react";
import { Image, WebView, View } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Left,
	Body,
	Right,
	Spinner
} from "native-base";
import styles from "./styles";

class ViewCurso extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			title: this.props.navigation.state.params.title.rendered,
			content: this.props.navigation.state.params.content.rendered
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Detalhes</Title>
					</Body>
					<Right />
				</Header>

				<Content padder>
					{this.state.content ?
						<View style={{ height: 540 }} >
							<WebView
								automaticallyAdjustContentInsets={false}
								source={{ html: this.state.content ? this.state.content : "<h2>Vazio</h2>" }}
								// source={{ uri: this.state.content ? this.state.content : "<h2>Vazio</h2>" }}
								contentInset={{ top: 0, right: 0, bottom: 0, left: 0 }}
								// style={{ height: 10000 }}
								javaScriptEnabled={true}
							// domStorageEnabled={true}
							// startInLoadingState={true}
							/>
						</View>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>

			</Container>
		);
	}
}

export default ViewCurso;
