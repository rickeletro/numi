import * as React from "react";
import { View, AsyncStorage, Image } from "react-native";
import { decode as atob } from "base-64";
import moment from "moment";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Card,
	CardItem,
	Left,
	Body,
	Right,
	List,
	Spinner
} from "native-base";
import styles from "./styles";
class Diario extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			datas: '',
			user: ''
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	getUserIdFromToken = (token) => {
		let decodedString = atob(token.split(".")[1]);
		return JSON.parse(decodedString).data.user.id;
	}

	fetchPosts = async () => {
		try {
			let userToken = await AsyncStorage.getItem('access_token');
			let userId = await this.getUserIdFromToken(userToken);
			let response = await fetch(
				'http://app.nucleonumi.com.br/wp-json/wp/v2/diario_de_praticas?author=' + userId
			);
			let resp = await response.json();
			await this.setState({
				datas: resp,
				user: userId
			})
		} catch (error) {
			console.error(error);
		}
	}

	deletePosts = async (id) => {
		try {
			var userToken = await AsyncStorage.getItem('access_token');
			let response = await fetch(
				'http://app.nucleonumi.com.br/wp-json/wp/v2/diario_de_praticas/' + id, {
					method: 'DELETE',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + userToken,
					}
				});

			let resp = await response.json();
			if (resp) {
				this.fetchPosts();
			}
		} catch (error) {
			console.error(error);
		}
	}

	componentWillMount() {
		this.fetchPosts();
	}

	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ flex: 1, alignItems: "center" }}>
						<Title>Diário</Title>
					</Body>
					<Right>
						<Button onPress={() => this.props.navigation.navigate('NovoPratica', {
							onGoBack: () => this.fetchPosts(), data: { title: { rendered: 'Pratica Livre' } }
						})}>
							<Text>Novo</Text>
						</Button>
					</Right>
				</Header>
				<Content padder>
					{this.state.datas ?
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<Card style={{ flex: 0 }}>
									<CardItem>
										<Left>
											<Body>
												<Text>{data.title.rendered}</Text>
												<Text note>{moment(data.date).utc().format("DD-MM-YYYY")}</Text>
											</Body>
										</Left>
									</CardItem>

									<CardItem bordered>
										<Body>
											<Text>
												{data.content.rendered.stripHTML()}
											</Text>
										</Body>
									</CardItem>

									<CardItem style={{ backgroundColor: '#ccc' }}>
										<Left>
											<Button transparent textStyle={{ color: '#87838B' }} onPress={() => this.props.navigation.navigate('EditDiario', {
												onGoBack: () => this.fetchPosts(), data
											})}>
												<Image
													source={require('../../../../assets/create.png')}
												/>
												<Text>Editar</Text>
											</Button>
										</Left>
										<Body>
											<Button transparent textStyle={{ color: '#87838B' }} onPress={() => this.deletePosts(data.id)} >
												<Image
													source={require('../../../../assets/trash.png')}
												/>
												<Text>Apagar</Text>
											</Button>
										</Body>
										<Right>
											<Text>{data.acf.crono_pratica}</Text>
										</Right>
									</CardItem>
								</Card>
							}
						/>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>

			</Container>
		);
	}
}

export default Diario;
