import * as React from "react";
import { Image } from "react-native";
import moment from "moment";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Card,
	CardItem,
	Text,
	Left,
	Body,
	Right,
	List,
	Spinner
} from "native-base";
import styles from "./styles";

class Blog extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			datas: ''
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	componentDidMount() {
		fetch('http://app.nucleonumi.com.br/wp-json/wp/v2/posts/?page=1&_embed')
			.then(response => {
				return response.json();
			})
			.then(resp => {
				this.setState({
					datas: resp
				});
			});
	}
	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Blog</Title>
					</Body>
					<Right />
				</Header>

				<Content padder>
				{this.state.datas ? 
					<List
						dataArray={this.state.datas}
						renderRow={data =>
							<Card style={{ flex: 0 }}>
								<CardItem>
									<Left>
										<Body>
											<Text>{data.title.rendered}</Text>
											<Text note>{moment(data.date).utc().format("DD-MM-YYYY")}</Text>
										</Body>
									</Left>
								</CardItem>

								<CardItem bordered>
									<Body>
										{data.acf.imagem_post ?
											<Image source={{ uri: data.acf.imagem_post.sizes.medium_large }} style={{ height: 189, width: '100%', flex: 1 }} />
											: <Image source={require('../../../../assets/nuvens.png')} style={{ height: 189, width: '100%', flex: 1 }} />
										}
										<Text>
											{data.excerpt.rendered.stripHTML()}
										</Text>
									</Body>
								</CardItem>
								<CardItem style={{ backgroundColor: '#ccc' }}>
									<Left>
										<Button transparent textStyle={{ color: '#87838B' }} onPress={() => this.props.navigation.navigate('ViewBlog', data)}>
											<Image
												source={require('../../../../assets/blog.png')}
											/>
											<Text>Ler mais ...</Text>
										</Button>
									</Left>
									<Body>

									</Body>
									<Right>

									</Right>
								</CardItem>
							</Card>
						}
					/>
					:
						<Spinner style={{marginTop: 40}} color='orange' /> 
					}
				</Content>
			</Container>
		);
	}
}

export default Blog;
