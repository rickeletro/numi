import * as React from "react";
import { Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Card,
	CardItem,
	Text,
	Left,
	Body,
	Right,
	List
} from "native-base";
import styles from "./styles";

class ViewBlog extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			title: this.props.navigation.state.params.title.rendered,
			imagem: this.props.navigation.state.params.acf.imagem_post.sizes.medium_large,
			content: this.props.navigation.state.params.content.rendered,
			date: this.props.navigation.state.params.date
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Post</Title>
					</Body>
					<Right />
				</Header>

				<Content padder>
					<Card style={{ flex: 0 }}>
						<CardItem>
							<Left>
								<Body>
									<Text>{this.state.title}</Text>
									<Text note>{this.state.date}</Text>
								</Body>
							</Left>
						</CardItem>

						<CardItem bordered>
							<Body>
								{this.props.navigation.state.params.acf.imagem_post ?
									<Image source={{ uri: this.state.imagem }} style={{ height: 189, width: '100%', flex: 1 }} />
									: <Image source={require('../../../../assets/nuvens.png')} style={{ height: 189, width: '100%', flex: 1 }} />
								}
								<Text>
									{this.state.content.stripHTML()}
								</Text>
							</Body>
						</CardItem>
					</Card>
				</Content>

			</Container>
		);
	}
}

export default ViewBlog;
