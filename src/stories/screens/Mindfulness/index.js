import * as React from "react";
import { Image, WebView, View } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Left,
	Body,
	Right,
	Spinner
} from "native-base";
import styles from "./styles";

class Mindfulness extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			datas: {
				content: {
					rendered: ''
				}
			}
		}
		String.prototype.stripHTML = function () { return this.replace(/<.*?>/g, ''); }
	}

	componentWillMount() {
		fetch('https://app.nucleonumi.com.br/wp-json/wp/v2/pages/?slug=mindfulness')
			.then(response => {
				return response.json();
			})
			.then(resp => {
				this.setState({
					datas: resp[0]
				});
			});
	}
	render() {
		return (
			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ flex: 3, alignItems: "center" }}>
						<Title>Mindfulness</Title>
					</Body>
					<Right />
				</Header>

				<Content padder>
					{this.state.datas.content.rendered ?
						<View style={{ height: 540 }} >
							<WebView
								automaticallyAdjustContentInsets={false}
								source={{ html: this.state.datas.content.rendered ? this.state.datas.content.rendered : "<h2></h2>" }}
								// scalesPageToFit
								contentInset={{ top: 0, right: 0, bottom: 0, left: 0 }}
								// style={{ height: 3000 }}
								javaScriptEnabled={true}
							/>
						</View>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>

			</Container>
		);
	}
}

export default Mindfulness;
