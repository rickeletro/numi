// @flow
import * as React from "react";
import NovoPratica from "../../stories/screens/NovoPratica";
export interface Props {
	navigation: any,
}
export interface State {}
export default class NovoPraticaContainer extends React.Component<Props, State> {
	render() {
		return <NovoPratica navigation={this.props.navigation} />;
	}
}
