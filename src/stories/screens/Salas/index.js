import * as React from "react";
import { Linking, Image } from "react-native";
import {
	Container,
	Header,
	Title,
	Content,
	Button,
	Text,
	Left,
	Body,
	Right,
	List,
	ListItem,
	Spinner
} from "native-base";
import styles from "./styles";

class Salas extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			datas: ''
		}
	}
	fetchPosts = async () => {
		try {
			let response = await fetch(
				'https://app.nucleonumi.com.br/wp-json/wp/v2/encontros_virtuais'
			);
			let resp = await response.json();
			this.setState({
				datas: resp
			})
		} catch (error) {
			console.error(error);
		}
	}
	componentDidMount() {
		this.fetchPosts();
	}
	render() {

		return (

			<Container style={styles.container}>
				<Header>
					<Left>
						<Button transparent onPress={() => this.props.navigation.goBack()}>
							<Image
								source={require('../../../../assets/left-arrow.png')}
							/>
						</Button>
					</Left>
					<Body style={{ alignItems: "center" }}>
						<Title>Salas Virtuais</Title>
					</Body>

					<Right />
				</Header>

				<Content padder>
					{this.state.datas ?
						<List
							dataArray={this.state.datas}
							renderRow={data =>
								<ListItem button
									onPress={() => {
										Linking.openURL(data.acf.encontro_sala);
									}}
								>
									<Left>
										<Text>{data.title.rendered}</Text>
									</Left>
									<Right>
										<Image
											source={require('../../../../assets/right-arrow-forward.png')}
										/>
									</Right>
								</ListItem>
							}
						/>
						:
						<Spinner style={{ marginTop: 40 }} color='orange' />
					}
				</Content>

			</Container>
		);
	}
}

export default Salas;
