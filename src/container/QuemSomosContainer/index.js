// @flow
import * as React from "react";
import QuemSomos from "../../stories/screens/QuemSomos";
export interface Props {
	navigation: any,
}
export interface State {}
export default class QuemSomosContainer extends React.Component<Props, State> {
	render() {
		return <QuemSomos navigation={this.props.navigation} />;
	}
}
