import React, { Component } from 'react';
import Logo from './components/Logo';
import Wallpaper from './components/Wallpaper';
import Dimensions from 'Dimensions';
import OneSignal from 'react-native-onesignal';
import { Button } from 'native-base';

import {
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Text,
  TextInput,
  Animated,
  Easing,
  Image,
  AsyncStorage,
  View,
} from 'react-native';
import usernameImg from '../../../../assets/username.png';
import passwordImg from '../../../../assets/password.png';
import eyeImg from '../../../../assets/eye_black.png';
import spinner from '../../../../assets/loading.gif';

const DEVICE_WIDTH = Dimensions.get('window').width;
const MARGIN = 40;
const ACCESS_TOKEN = 'access_token';
export default class Login extends Component {
  static navigationOptions = {
    headerStyle: {
      display: 'none'
    }
  }
  constructor() {
    super();
    this.state = {
      isLoading: false,
      showPass: true,
      press: false,
      user: "",
      pass: "",
      error: "",
    };
    this._bootstrapAsync();
    this.buttonAnimated = new Animated.Value(0);
    this.growAnimated = new Animated.Value(0);
    this._onPress = this._onPress.bind(this);
    this.showPass = this.showPass.bind(this);
  }


  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('access_token');
    this.props.navigation.navigate(userToken ? 'Home' : 'Login');

  };

  storeToken(responseData) {
    AsyncStorage.setItem(ACCESS_TOKEN, responseData.token, (err) => {
      if (err) {
        console.log("an error");
        throw err;
      }
    }).catch((err) => {
      console.log("error is: " + err);
    });
    AsyncStorage.setItem('author_email', responseData.user_email);
    AsyncStorage.setItem('author_name', responseData.user_nicename);
  }

  componentWillMount() {
    OneSignal.init("6f6a24d5-28ea-4950-bfcc-56d0312a61cc");

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    // console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    // console.log('Message: ', openResult.notification.payload.body);
    // console.log('Data: ', openResult.notification.payload.additionalData);
    // console.log('isActive: ', openResult.notification.isAppInFocus);
    // console.log('openResult: ', openResult);
  }

  async _onPress() {
    if (this.state.isLoading) return;
    this.setState({ isLoading: true });
    Animated.timing(this.buttonAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();

    setTimeout(() => {
      this._onGrow();
    }, 2000);

    try {
      let response = await fetch('https://app.nucleonumi.com.br/wp-json/jwt-auth/v1/token', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: this.state.user,
          password: this.state.pass,
        })
      });

      let res = await response.text();
      let resp = JSON.parse(res);
      // Se logado
      if (resp.token) {
        //Handle success
        // let accessToken = resp.token;
        let accessToken = resp;
        // On success we will store the access_token in the AsyncStorage
        this.storeToken(accessToken);
        // Se logado é redirecionado
        this.props.navigation.navigate('Home');
        this.setState({ isLoading: false });
        this.buttonAnimated.setValue(0);
        this.growAnimated.setValue(0);
      } else {
        this.setState({ isLoading: false });
        this.buttonAnimated.setValue(0);
        this.growAnimated.setValue(0);
        alert("Login ou Senha não existe")
        //Handle error
        let error = resp;
        throw error;
      }
    } catch (error) {
      this.setState({ error: error });
      this.setState({ isLoading: false });
      this.buttonAnimated.setValue(0);
      this.growAnimated.setValue(0);
    }
  }

  _onGrow() {
    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 200,
      easing: Easing.linear,
    }).start();
  }

  showPass() {
    this.state.press === false
      ? this.setState({ showPass: false, press: true })
      : this.setState({ showPass: true, press: false });
  }
  _onResetPass() {
    this.props.navigation.navigate('RecuperarSenha');
  }
  // RENDER
  render() {
    const changeWidth = this.buttonAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [DEVICE_WIDTH - MARGIN, MARGIN],
    });
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, MARGIN],
    });
    return (
      <Wallpaper>
        <Logo />
        <KeyboardAvoidingView behavior="padding" contentContainerStyle={styles.teclado} style={styles.containerForm} enabled>
          <View style={styles.inputWrapper}>
            <Image source={usernameImg} style={styles.inlineImg} />
            <TextInput
              style={styles.input}
              source={usernameImg}
              placeholder="Usuario"
              autoCorrect={false}
              autoCapitalize={'none'}
              returnKeyType={'done'}
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
              onChangeText={(text) => this.setState({ user: text })}
            />
          </View>
          <View style={styles.inputWrapper}>
            <Image source={passwordImg} style={styles.inlineImg} />
            <TextInput
              style={styles.input}
              source={passwordImg}
              placeholder="Senha"
              secureTextEntry={this.state.showPass}
              autoCorrect={false}
              autoCapitalize={'none'}
              returnKeyType={'done'}
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
              onChangeText={(text) => this.setState({ pass: text })}
            />
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.btnEye}
            onPress={this.showPass}>
            <Image source={eyeImg} style={styles.iconEye} />
          </TouchableOpacity>
        </KeyboardAvoidingView>

        <View style={styles.containerTouch}>
          <Animated.View style={{ width: changeWidth }}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => { this._onPress() }}
              activeOpacity={1}>
              {this.state.isLoading ? (
                <Image source={spinner} style={styles.image} />
              ) : (
                  <Text style={styles.text}>LOGIN</Text>
                )}
            </TouchableOpacity>
            <Animated.View
              style={[styles.circle, { transform: [{ scale: changeScale }] }]}
            />
          </Animated.View>
        </View>

        <View style={styles.container}>
          <Button transparent
            onPress={() => {
              alert('Entre em contato com o núcleo de mindfulness e solicite seu cadastro.')
            }}>
            <Text style={styles.text}>Criar conta</Text>
          </Button >
          <Button transparent
            onPress={() => {
              this._onResetPass() // Linking.openURL("https://app.nucleonumi.com.br/minha-conta/lost-password/");
            }}>
            <Text style={styles.text}>Esqueceu a senha?</Text>
          </Button >
        </View>

      </Wallpaper>
    );
  }
}


// STYLE
const styles = StyleSheet.create({
  containerTouch: {
    flex: 1,
    // top: -95,
    alignItems: 'center',
    // justifyContent: 'flex-start',
  },
  containerForm: {
    flex: 1,
    alignItems: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#EDA04C',
    height: MARGIN,
    borderRadius: 20,
    zIndex: 100,
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#EDA04C',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#EDA04C',
  },
  text: {
    color: 'white',
    backgroundColor: 'transparent',
  },
  image: {
    width: 24,
    height: 24,
  },
  btnEye: {
    position: 'absolute',
    top: 55,
    right: 28,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.2)',
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    width: DEVICE_WIDTH - 40,
    height: 40,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#ffffff',
  },
  inputWrapper: {
    // flex: 1,
    paddingTop: 5,
  },
  inlineImg: {
    position: 'absolute',
    zIndex: 99,
    width: 22,
    height: 22,
    left: 35,
    top: 9,
  },
  container: {
    // flex: 1,
    // top: 20,
    width: DEVICE_WIDTH,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});